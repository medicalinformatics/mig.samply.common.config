# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)

## [4.0.5] - 2022-01-11
### Security
- Updated Log4j (CVE-2021-44832).

## [4.0.4] - 2021-12-18
### Security
- Updated Log4j (CVE-2021-45105) and SLF4J (CVE-2018-8088) to their latest versions.

## [4.0.3] - 2021-12-15
### Security
- Updated Log4j (CVE-2021-45046)

## [4.0.2] - 2021-12-10
### Security
- Updated Log4j to fix potential remote code execution vulnerability.

## [4.0.1] - 2020-05-26
### Changed
- no relevant changes in OS release.

## [4.0.0] - 2020-05-26
### Changed
- apply google codestyle
- update parent.pom

## [3.0.0] - 2018-12-14
### Added
- initial release